import datetime
import os
from time import sleep
import pandas as pd
import numpy as np
import csv
import itertools
import statsmodels.api as sm
from simplecrypt import encrypt,decrypt
#from cryptography.fernet import Fernet

from finam.export import Exporter, Market, Timeframe #, LookupComparator
exporter = Exporter()
import dropbox
# Create a dropbox object using an API v2 key
d = dropbox.Dropbox('HE-P_31nZcIAAAAAAAAFHA7Ie1qwrAmrlecFCifSGVY4ETqMNgSLIX1yKQUADh3M')


from sklearn import model_selection, metrics
#from sklearn.utils import resample
from sklearn.preprocessing import StandardScaler,OneHotEncoder
from sklearn.feature_selection import VarianceThreshold, SelectKBest, SelectFromModel
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.linear_model import Lasso
from sklearn.linear_model import LinearRegression
from sklearn.compose import ColumnTransformer,make_column_selector
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from scipy.stats import randint, uniform
import numpy as np



def db_upload(df,filename):
    to_db = df.to_csv(index=False)
    f= 'qivb#eroiau*232u9rh&9fdi'
    ciphertext = encrypt(f,to_db)#.encode('ascii')
    meta = d.files_upload(ciphertext, '/results/'+filename, mode=dropbox.files.WriteMode("overwrite"))


def map_data_daily(df,ref_date = datetime.datetime.today().date()):
    df['datetime'] = df.index

    def get_dayofweek(row):
        return datetime.datetime.strptime(row['datetime'], "%Y-%m-%d %H:%M:%S").weekday()

    def get_time(row):
        delta = datetime.datetime.combine(datetime.datetime(1, 1, 1, 0, 0, 0),
                                          datetime.datetime.strptime(row['datetime'],
                                                                     "%Y-%m-%d %H:%M:%S").time()) - datetime.datetime.combine(
            datetime.datetime(1, 1, 1, 0, 0, 0), datetime.time(16, 30))
        return delta.seconds

    def get_date(row):
        delta = datetime.datetime.strptime(row['datetime'], "%Y-%m-%d %H:%M:%S").date()
        return delta

    def get_days_since(row):
        delta = ref_date - row['date']
        return delta.days

    df['dayofweek'] = df.apply(get_dayofweek, axis=1)
    df['time'] = df.apply(get_time, axis=1)
    df['date'] = df.apply(get_date, axis=1)
    df = df[df['date'] < ref_date]
    close = df.copy()
    close = close[['date', '<CLOSE>']]
    close.columns = ['date', 'prev_close']
    close = close.drop_duplicates(subset='date', keep='last')
    first = close['prev_close'].iloc[0]
    close['prev_close'] = close['prev_close'].shift(1).fillna(first)
    df = df.merge(close, on='date')
    df['date'] = df.apply(get_days_since, axis=1)
    df['up'] = df['<HIGH>'] - df['prev_close']
    df['down'] = df['<LOW>'] - df['prev_close']
    return df[df['date'] <= 30]


def check_accuracy_container(df,ref_date=datetime.datetime.today().date()):
    def get_date(row):
        delta = datetime.datetime.strptime(row['datetime'],"%Y-%m-%d %H:%M:%S").date() - ref_date
        return delta.days

    def check_accuracy(row):
        stock = row['stock']
        try:
            data = pd.read_csv('./raw_data/stocks/alpha_vantage/'+stock+'.csv',index_col='timestamp')
            data = data[['open', 'high', 'low', 'close', 'volume']]
            data = data[data['volume']==df['volume']]
            data.columns = ['<OPEN>','<HIGH>','<LOW>','<CLOSE>','<VOL>']
        except:
            return np.nan, np.nan, np.nan

        data['datetime'] = data.index
        data.reset_index(drop=True,inplace=True)
        data['temp_date'] = data.apply(get_date, axis=1)
        data = data[(data['temp_date'] > 0) & (data['temp_date'] < 7)]
        if len(data) == 0:
            return np.nan,np.nan,np.nan
        else:
            buy = data[data['<LOW>'] <= row['min_5'] + row['present_val']].head(1)
            if len(buy) == 0:
                return max(data['<HIGH>']), min(data['<LOW>']), 0
            else:
                sell = data[(data['<HIGH>'] >= row['max_95'] + row['present_val']) & (data.index >= buy.index[0])].head(1)
                if len(sell) == 0:
                    return max(data['<HIGH>']), min(data['<LOW>']), -1
                else:
                    return max(data['<HIGH>']), min(data['<LOW>']), 1

    df['actual_high'], df['actual_low'], df['worked'] = zip(*df.apply(check_accuracy,axis=1))
    return df


def map_data(df,ref_date = datetime.datetime.today().date()):
    df['datetime'] = df.index

    def get_dayofweek(row):
        return datetime.datetime.strptime(row['datetime'], "%Y-%m-%d %H:%M:%S").weekday()

    def get_time(row):
        delta = datetime.datetime.combine(datetime.datetime(1, 1, 1, 0, 0, 0),
                                          datetime.datetime.strptime(row['datetime'],
                                                                     "%Y-%m-%d %H:%M:%S").time()) - datetime.datetime.combine(
            datetime.datetime(1, 1, 1, 0, 0, 0), datetime.time(16, 30))
        return delta.seconds

    def get_date(row):
        delta = datetime.datetime.strptime(row['datetime'], "%Y-%m-%d %H:%M:%S").date()
        return delta

    def get_days_since(row):
        delta = ref_date - row['date']
        return delta.days

    df['dayofweek'] = df.apply(get_dayofweek, axis=1)
    df['time'] = df.apply(get_time, axis=1)
    df['date'] = df.apply(get_date, axis=1)
    df = df[df['date'] < ref_date]
    close = df.copy()
    close = close[close['dayofweek'] == 4]
    close = close[['date', '<CLOSE>']]
    close.columns = ['date', 'prev_close']
    close = close.drop_duplicates(subset='date', keep='last')
    first = close['prev_close'].iloc[0]
    close['prev_close'] = close['prev_close'].shift(1).fillna(first)
    df = df.merge(close, on='date')
    df['date'] = df.apply(get_days_since, axis=1)
    df['up'] = df['<HIGH>'] - df['prev_close']
    df['down'] = df['<LOW>'] - df['prev_close']
    return df[df['date'] <= 30]


def model_stock(df, name):
    SEED = 100
    NFOLDS = 5
    RAND_N_ITER = 100

    numeric_features = make_column_selector(dtype_include='number')
    numeric_transformer = Pipeline(steps=[
        ('imputer', SimpleImputer(strategy='constant',fill_value=-1)),
        ('scaler',StandardScaler())
    ])

    categorical_features = make_column_selector(dtype_include=object)
    categorical_transformer = Pipeline(steps=[
        ('imputer', SimpleImputer(strategy='constant',fill_value=-1)),
        ('onehot',OneHotEncoder(handle_unknown='ignore'))
    ])

    preprocessor = ColumnTransformer(
        transformers = [
            ('num',numeric_transformer,numeric_features),
            ('cat',categorical_transformer,categorical_features)
        ],
        remainder='passthrough',
        n_jobs=-1
    )
    
    KFOLD = model_selection.KFold(n_splits=NFOLDS,
                                  random_state=SEED,
                                  shuffle=True)
    RAND_PARAM_GRID = {'gbr__loss': ['ls', 'lad', 'huber', 'quantile'],
                       'gbr__learning_rate': [0.1],
                       'gbr__n_estimators': randint(1, 25),
                       'gbr__subsample': uniform(loc=.3, scale=.7),

                       'gbr__max_leaf_nodes': randint(2, 5),
                       'gbr__min_samples_split': randint(2, 10),
                       'gbr__max_depth': randint(2, 5),
                       'gbr__max_features': [None, 'auto']
                       }

    pipe = Pipeline([
        ('preprocessor',preprocessor),
        #('scaler', StandardScaler()),
        # ('feature_selection', SelectFromModel(estimator=Lasso(random_state=SEED), threshold="1.25*median")),
        ('gbr', GradientBoostingRegressor())
    ])

    clf_high = model_selection.RandomizedSearchCV(pipe,
                                                  param_distributions=RAND_PARAM_GRID,
                                                  cv=KFOLD,
                                                  n_jobs=-1,
                                                  #iid=False,
                                                  return_train_score=True,
                                                  verbose=0,
                                                  n_iter=RAND_N_ITER
                                                  )

    clf_low = model_selection.RandomizedSearchCV(pipe,
                                                 param_distributions=RAND_PARAM_GRID,
                                                 cv=KFOLD,
                                                 n_jobs=-1,
                                                 #iid=False,
                                                 return_train_score=True,
                                                 verbose=0,
                                                 n_iter=RAND_N_ITER
                                                 )

    model_features = ['dayofweek', 'time', '<VOL>', 'prev_close', 'date']
    clf_high.fit(df[model_features], df['up'])
    clf_low.fit(df[model_features], df['down'])

    # print("Best Test Score: " + str(clf_high.best_score_))
    trend_df = df.drop_duplicates(subset='prev_close')
    model = LinearRegression().fit(np.array(list(range(len(trend_df)))).reshape((-1, 1)), np.array(trend_df['prev_close']))

    present_val = df['<CLOSE>'].iloc[len(df) - 1]
    scoringset = pd.DataFrame()
    scoringset['dayofweek'] = sorted([0, 1, 2, 3, 4] * 23340)
    scoringset['date'] = sorted([-1, -2, -3, -4, -5] * 23340)
    scoringset['time'] = list(range(0, 23340)) * 5


    hist = np.histogram(df['<VOL>'], bins=20)
    scale = list(hist[0] / sum(hist[0])) + [0]
    # projectedmax = projectedmin = 0
    # projectedmax_trended = projectedmin_trended = 0

    avg_activity = len(df)/len(df.drop_duplicates(subset='prev_close'))
    avg_volume = float(df['<VOL>'].mean())
    out = {'stock':name, 'r2':(clf_high.best_score_ + clf_high.best_score_)/2, 'r2_std':(clf_high.cv_results_['std_test_score'][clf_high.best_index_]**2+clf_low.cv_results_['std_test_score'][clf_low.best_index_]**2)/2,'trend':model.coef_[0],'avg_activity':avg_activity,'avg_vol':avg_volume,'present_val':present_val}

    projected_res = {}
    for j in range(0,105,5):
        projected_res['max_' + str(j)] = 0
        projected_res['min_' + str(j)] = 0
        projected_res['trend_max_' + str(j)] = 0
        projected_res['trend_min_' + str(j)] = 0
        projected_res['gain' + str(j)] = 0
        projected_res['trend_gain_' + str(j)] = 0
    for i in range(len(hist[1])):
        if scale[i]==0:
            continue
        scoringset['<VOL>'] = hist[1][i]
        # predict max and min
        # incorporate probability
        # outlier system needed
        # add news
        scoringset['prev_close'] = present_val
        scoringset['scores_high_' + str(hist[1][i])] = clf_high.predict(scoringset[model_features])
        scoringset['scores_low_' + str(hist[1][i])] = clf_low.predict(scoringset[model_features])
        for j in range(0,105,5):
            projected_res['max_'+str(j)] = projected_res['max_'+str(j)] + scale[i] * scoringset['scores_high_' + str(hist[1][i])].quantile(q=j/100)
            projected_res['min_'+str(j)] = projected_res['min_'+str(j)] + scale[i] * scoringset['scores_low_' + str(hist[1][i])].quantile(q=j/100)

        scoringset['prev_close'] = (scoringset['dayofweek'] + 1) * model.coef_[0] + present_val
        scoringset['scores_high_' + str(hist[1][i])] = clf_high.predict(scoringset[model_features])
        scoringset['scores_low_' + str(hist[1][i])] = clf_low.predict(scoringset[model_features])
        for j in range(0, 105, 5):
            projected_res['trend_max_'+str(j)] = projected_res['trend_max_'+str(j)] + scale[i] * scoringset['scores_high_' + str(hist[1][i])].quantile(q=j/100)
            projected_res['trend_min_'+str(j)] = projected_res['trend_min_'+str(j)] + scale[i] * scoringset['scores_low_' + str(hist[1][i])].quantile(q=j/100)

    for j in range(0,105,5):
        projected_res['gain' + str(j)] = (projected_res['max_' + str(j)] - projected_res['min_' + str(100-j)]) / (projected_res['min_' + str(100-j)] + present_val)
        projected_res['trend_gain_'+str(j)] = (projected_res['trend_max_'+str(j)] - projected_res['trend_min_'+str(100-j)]) / (projected_res['trend_min_'+str(100-j)]+present_val)

    out.update(projected_res)
    return out

# Classifier functions
model_features = ['trend',
 'avg_activity',
 'gain0',
 'gain10',
 'gain100',
 'gain15',
 'gain20',
 'gain25',
 'gain30',
 'gain35',
 'gain40',
 'gain45',
 'gain5',
 'gain50',
 'gain55',
 'gain60',
 'gain65',
 'gain70',
 'gain75',
 'gain80',
 'gain85',
 'gain90',
 'gain95',
 'max_0',
 'max_10',
 'max_100',
 'max_15',
 'max_20',
 'max_25',
 'max_30',
 'max_35',
 'max_40',
 'max_45',
 'max_5',
 'max_50',
 'max_55',
 'max_60',
 'max_65',
 'max_70',
 'max_75',
 'max_80',
 'max_85',
 'max_90',
 'max_95',
 'min_0',
 'min_10',
 'min_100',
 'min_15',
 'min_20',
 'min_25',
 'min_30',
 'min_35',
 'min_40',
 'min_45',
 'min_5',
 'min_50',
 'min_55',
 'min_60',
 'min_65',
 'min_70',
 'min_75',
 'min_80',
 'min_85',
 'min_90',
 'min_95',
 'present_val',
 'r2',
 'r2_std',
 'trend_gain_0',
 'trend_gain_10',
 'trend_gain_100',
 'trend_gain_15',
 'trend_gain_20',
 'trend_gain_25',
 'trend_gain_30',
 'trend_gain_35',
 'trend_gain_40',
 'trend_gain_45',
 'trend_gain_5',
 'trend_gain_50',
 'trend_gain_55',
 'trend_gain_60',
 'trend_gain_65',
 'trend_gain_70',
 'trend_gain_75',
 'trend_gain_80',
 'trend_gain_85',
 'trend_gain_90',
 'trend_gain_95',
 'trend_max_0',
 'trend_max_10',
 'trend_max_100',
 'trend_max_15',
 'trend_max_20',
 'trend_max_25',
 'trend_max_30',
 'trend_max_35',
 'trend_max_40',
 'trend_max_45',
 'trend_max_5',
 'trend_max_50',
 'trend_max_55',
 'trend_max_60',
 'trend_max_65',
 'trend_max_70',
 'trend_max_75',
 'trend_max_80',
 'trend_max_85',
 'trend_max_90',
 'trend_max_95',
 'trend_min_0',
 'trend_min_10',
 'trend_min_100',
 'trend_min_15',
 'trend_min_20',
 'trend_min_25',
 'trend_min_30',
 'trend_min_35',
 'trend_min_40',
 'trend_min_45',
 'trend_min_5',
 'trend_min_50',
 'trend_min_55',
 'trend_min_60',
 'trend_min_65',
 'trend_min_70',
 'trend_min_75',
 'trend_min_80',
 'trend_min_85',
 'trend_min_90',
 'trend_min_95',
 'IPOyear',
 'Sector',
 'industry',
 'shares',
 'cap',
                  #'week_count'
                  ]

def classifier_builder(data,scoring_set):
    SEED = 100
    NFOLDS = 10
    KFOLD = model_selection.KFold(n_splits=NFOLDS,
                                  random_state=SEED,
                                  shuffle=True)

    RAND_N_ITER = 50
    RAND_PARAM_GRID = {'gbc__n_estimators': randint(10, 120),
                       'gbc__learning_rate': [0.1],
                       'gbc__max_leaf_nodes': randint(2, 5),
                       'gbc__min_samples_split': randint(2, 10),
                       'gbc__max_depth': randint(2, 5),
                       'gbc__subsample': uniform(loc=.3, scale=.7),  # min=loc, max=loc + scale
                       'gbc__max_features': [None, 'auto']}

    pipe = Pipeline([
        ('scaler', StandardScaler()),
        # ('feature_selection', SelectFromModel(estimator=RandomForestClassifier(random_state=SEED), threshold="1.25*median")),
        ('gbc', GradientBoostingClassifier())
    ])

    clf = model_selection.RandomizedSearchCV(pipe,
                                             param_distributions=RAND_PARAM_GRID,
                                             cv=KFOLD,
                                             n_jobs=-1,
                                             #iid=False,
                                             return_train_score=True,
                                             verbose=1,
                                             n_iter=RAND_N_ITER
                                             )
    model_df = data[model_features]
    model_df = pd.get_dummies(model_df)
    model_df = model_df.fillna(-1)

    clf.fit(model_df, data['target'])
    print("Best Test Score: " + str(clf.best_score_))
    print("Best Test Std: " + str(clf.cv_results_['std_test_score'][clf.best_index_]))
    scoring_set_clean = scoring_set[model_features]
    scoring_set_clean = pd.get_dummies(scoring_set_clean)
    scoring_set_clean = scoring_set_clean.fillna(-1)
    scoring_set['score'] = pd.DataFrame(clf.predict_proba(scoring_set_clean), columns=clf.classes_, index=scoring_set.index)[1]
    scoring_set = scoring_set.sort_values(by='score',ascending=False)
    return scoring_set

